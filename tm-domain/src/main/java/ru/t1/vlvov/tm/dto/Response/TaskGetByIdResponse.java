package ru.t1.vlvov.tm.dto.Response;

import lombok.NoArgsConstructor;
import ru.t1.vlvov.tm.model.Task;

@NoArgsConstructor
public final class TaskGetByIdResponse extends AbstractTaskResponse {

    public TaskGetByIdResponse(Task task) {
        super(task);
    }

}
