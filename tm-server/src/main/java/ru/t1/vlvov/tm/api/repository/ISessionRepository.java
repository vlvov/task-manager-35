package ru.t1.vlvov.tm.api.repository;

import ru.t1.vlvov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
