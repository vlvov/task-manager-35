package ru.t1.vlvov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.repository.ITaskRepository;
import ru.t1.vlvov.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String projectId) {
        return models.stream()
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }

}
