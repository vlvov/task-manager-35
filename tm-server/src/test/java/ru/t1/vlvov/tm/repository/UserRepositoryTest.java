package ru.t1.vlvov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.repository.IUserRepository;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.model.User;
import ru.t1.vlvov.tm.service.PropertyService;
import ru.t1.vlvov.tm.util.HashUtil;
import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository(propertyService);

    @After
    public void tearDown() {
        userRepository.clear();
    }

    @Test
    public void add() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        Assert.assertEquals(USER1, userRepository.findAll().get(0));
    }

    @Test
    public void addList() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER_LIST);
        Assert.assertEquals(USER_LIST, userRepository.findAll());
    }

    @Test
    public void set() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER_LIST);
        userRepository.set(USER_LIST2);
        Assert.assertEquals(USER_LIST2, userRepository.findAll());
    }

    @Test
    public void clear() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER_LIST);
        Assert.assertFalse(userRepository.findAll().isEmpty());
        userRepository.clear();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void findAll() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER_LIST);
        Assert.assertEquals(USER_LIST, userRepository.findAll());
    }

    @Test
    public void findOneById() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER_LIST);
        Assert.assertEquals(USER1, userRepository.findOneById(USER1.getId()));
    }

    @Test
    public void remove() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        Assert.assertFalse(userRepository.findAll().isEmpty());
        userRepository.remove(USER1);
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeById() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        Assert.assertFalse(userRepository.findAll().isEmpty());
        userRepository.removeById(USER1.getId());
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void existsById() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        Assert.assertTrue(userRepository.existsById(USER1.getId()));
        Assert.assertFalse(userRepository.existsById(USER2.getId()));
    }

    @Test
    public void createLoginPassword() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        @NotNull User user = userRepository.create("TEST_USER1", "password1");
        Assert.assertTrue(userRepository.existsById(user.getId()));
        Assert.assertEquals("TEST_USER1", user.getLogin());
        Assert.assertEquals(HashUtil.salt("password1", new PropertyService()), user.getPasswordHash());
        Assert.assertNull(user.getEmail());
    }

    @Test
    public void createLoginPasswordEmail() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        @NotNull User user = userRepository.create("TEST_USER1", "password1", "test@test");
        Assert.assertTrue(userRepository.existsById(user.getId()));
        Assert.assertEquals("TEST_USER1", user.getLogin());
        Assert.assertEquals(HashUtil.salt("password1", new PropertyService()), user.getPasswordHash());
        Assert.assertEquals("test@test", user.getEmail());
    }

    @Test
    public void createLoginPasswordRole() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        @NotNull User user = userRepository.create("TEST_USER1", "password1", Role.USUAL);
        Assert.assertTrue(userRepository.existsById(user.getId()));
        Assert.assertEquals("TEST_USER1", user.getLogin());
        Assert.assertEquals(HashUtil.salt("password1", new PropertyService()), user.getPasswordHash());
        Assert.assertEquals(Role.USUAL, user.getRole());
        Assert.assertNull(user.getEmail());
    }

    @Test
    public void findByLogin() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        @NotNull User user = userRepository.create("TEST_USER1", "password1", "test@test");
        Assert.assertEquals(user, userRepository.findByLogin(user.getLogin()));
    }

    @Test
    public void findByEmail() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        @NotNull User user = userRepository.create("TEST_USER1", "password1", "test@test");
        Assert.assertEquals(user, userRepository.findByEmail(user.getEmail()));
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        USER1.setLogin("USER1");
        Assert.assertTrue(userRepository.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(userRepository.isLoginExist("NOT_EXIST_LOGIN"));
    }

    @Test
    public void isEmailExist() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        USER1.setEmail("test@test");
        Assert.assertTrue(userRepository.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(userRepository.isEmailExist("NOT_EXIST_EMAIL"));
    }

}
